package pl.mksiazek.helloworld.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class HelloWorldController {

    @GetMapping
    public Mono<ResponseEntity> hello() {
        return Mono.just(ResponseEntity.ok("Hello world"));
    }
}
